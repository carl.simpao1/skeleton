import math
from unittest import TestCase
def sqrt(num):
    return math.sqrt(num)
def add(a, b):
    return a+b

def divide(a, b):

    try:
        result = float(a)/float(b)
    except ZeroDivisionError as z:
        return "Cannot divide by zero"


        
    return result


class test_math(TestCase):
    def test_add(self):
        result = add(5, 2)
        self.assertEqual(result, 7)
    def test_sqrt(self):
        result = sqrt(9)
        self.assertEqual(result, 3)

    def test_divide(self):
        result = divide(8,4)
        self.assertEqual(result, 2)

        result = divide("8","4")
        self.assertEqual(result, 2)

        result = divide(4,0)
        self.assertEqual(result,"Cannot divide by zero")
