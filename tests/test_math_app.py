from unittest import TestCase
from my_tdd_app.app import app, model
import json


class MathAppTest(TestCase):
    """
    Test the math app.
    """

    def setUp(self):
        """
        Setup the app and data model.
        """
        self.app = app

        # This is some magic to get a Flask context
        ctx = self.app.app_context()
        ctx.push()

        # Setup the database and model
        model.db.init_app(app)
        model.db.create_all()
        model.db.session.commit()

        # Setup the client that we will use in the tests
        self.client = self.app.test_client()

    def test_add(self):
        """
        Test the /math/add url.
        """

        # Send an HTTP request to the application
        resp = self.client.get('/math/add?a=2&b=3')

        # Load the JSON response into a python dictionary
        resp_data = json.loads(resp.get_data(as_text=True))

        # Now test the resulting data
        self.assertEqual(resp_data['result'], 5)

    def test_sqrt(self):

        resp = self.client.get('/math/sqrt', query_string=dict(a='9'))

        resp_data = json.loads(resp.get_data())

        self.assertEqual(resp_data['result'], 3)

    def test_divide(self):
        """
        Test the /math/divide url.
        """

        # Send an HTTP request to the application
        resp = self.client.get('/math/divide?a=8&b=4')

        # Load the JSON response into a python dictionary
        resp_data = json.loads(resp.get_data(as_text=True))

        # Now test the resulting data
        self.assertEqual(resp_data['result'], 2)



