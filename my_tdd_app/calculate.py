import math


def add(a, b):
    # Add two numbers and return the result
    return float(a)+float(b)


def sqrt(num):
    # Return the square root of a number
    return math.sqrt(float(num))

def divide(a, b):

    try:
        result = float(a)/float(b)
    except ZeroDivisionError as z:
        return "Cannot divide by zero"
    return result